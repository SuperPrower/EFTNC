This is the repository of the five students of the
Innopolis University who decided to create a
clone of the old school game called "Gun.Smoke" in
techno-necromancy setting to learn about Entity-Component
system and other tricks of more advanced game development.

Project uses [JSON library by nlohmann](https://github.com/nlohmann/json)

**System requirements:**

Test builds are not available at the time.

To compile code, you will need CMake, SFML libraries and 
C++ compiler with support of C++14 or newer.