//
// Created by Alex on 28.12.2017.
//

#include "EntityManager.hpp"

#include <fstream>

eftnc::entity_t EntityManager::createBlank() {
	if (freeIDsTop != 0) {
		return freeIDs[--freeIDsTop];
	}

	++lastID;
	// Deal with overflow, since 0 is reserved for invalid entities
	// In this particular case, overflow should not be problematic.
	// If we manage to get to the MAX_UINT, then there is either a bug
	// 	or the game has been running for a long time and the earliest entities are probably deleted by that time
	if (lastID == eftnc::invalid_entity)
		lastID = eftnc::entity_t(eftnc::first_entity);
	return lastID;
}

eftnc::entity_t EntityManager::createFromSchema(const std::string &schema) {
	eftnc::entity_t entity = createBlank();

	nlohmann::json json;
	std::ifstream input("resources/schemas/" + schema);

	if (!input.good()) {
		throw eftnc::generic_exception("Failed to load a schema file!");
	}

	input >> json;

	for (auto iter : json) {
		// TODO : Add an invalid system for this particular case?
		std::string systemName = iter.value("id", "NULL");
		std::transform(systemName.begin(), systemName.end(), systemName.begin(), ::tolower);
		AbstractSystem * sys = sm->getSystem(systemName);
		nlohmann::json::iterator ctor = iter.find("params");
		if (ctor != iter.end())
			sys->assemble(entity, ctor.value());
		else
			sys->assemble(entity);
	}

	input.close();
	return entity;
}

void EntityManager::deleteEntity(eftnc::entity_t entity) {
	deleted.insert(entity);
}

void EntityManager::cleanup() {
	std::vector<AbstractSystem *> systems = sm->getAllSystems();
	for (auto iter:deleted) {
		for (auto sys:systems)
			if (sys->hasComponent(iter))
				sys->deleteComponent(iter);
		if (freeIDsTop != FREE_ID_STACK_SIZE)
			freeIDs[freeIDsTop++] = iter;
	}
	deleted.clear();
}
