//
// Created by Alex on 28.12.2017.
//

#ifndef EFTNC_ENTITYMANAGER_HPP
#define EFTNC_ENTITYMANAGER_HPP

#include "Types.hpp"
#include "systems/AbstractSystem.hpp"
#include <vector>
#include <set>

class EntityManager {
public:
	// NOTE : Not sure about this one
	EntityManager(SystemManager * sm) : freeIDsTop(0), lastID(0) {
		this->sm = sm;
	};

	eftnc::entity_t createBlank();

	/* TODO : Decide how to store schemas.
	 * It would be moe efficient to store schemas together,
	 * Then store all load requests and group requests from the same file together.
	 * This way, there would be significantly less disk I/O, which is good.
	 */
	/*
	 * A schema is defined as a JSON array of JSON objects.\
	 * Each object must have at least one field - "id", containing the id of the needed system.
	 * Other fields needed for creation of a new component should be present
	 * 	in a field called "construction" as an object.
	 */
	eftnc::entity_t createFromSchema(const std::string &);

	// Schedule an entity for deletion
	void deleteEntity(eftnc::entity_t);

	// Actually delete entities scheduled for deletion
	void cleanup();

private:
	const static unsigned int FREE_ID_STACK_SIZE = 100;

	SystemManager * sm;

	eftnc::entity_t freeIDs[FREE_ID_STACK_SIZE];
	unsigned int freeIDsTop;
	std::set<eftnc::entity_t> deleted;
	eftnc::entity_t lastID;
};

#endif //EFTNC_ENTITYMANAGER_HPP
