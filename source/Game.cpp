#include <systems/PositionSystem.hpp>
#include <systems/VelocitySystem.hpp>
#include <systems/GraphicsSystem.hpp>
#include <systems/CollisionSystem.hpp>
#include "Game.hpp"
#include "Shared.hpp"
#include "states/LevelState.hpp"

Game::Game() : entityManager(&systemManager), groupManager(200, 10) {
	window.create(sf::VideoMode(800, 600), "Escape from Techno-Necromancer's Castle");
	window.setFramerateLimit(30);

	SharedData::renderTo = &window;
	SharedData::referenceWindow = &window;
	SharedData::entityManager = &entityManager;
	SharedData::systemManager = &systemManager;
	SharedData::graphicResourcesManager = &m_graphicsMan;
	SharedData::groupManager = &groupManager;
	SharedData::audioResourcesManager = &m_audioMan;

	auto pos = new PositionSystem();
	auto vel = new VelocitySystem();
	auto graphics = new GraphicsSystem(&m_graphicsMan);
	auto collision = new CollisionSystem();
	auto controls = new ControlsSystem();

	pos->onCreate(&systemManager);
	vel->onCreate(&systemManager);
	graphics->onCreate(&systemManager);
	collision->onCreate(&systemManager);
	controls->onCreate(&systemManager);
}

int Game::gameLoop() {
	clock.restart();

	StateManager sm;
	sm.pushState(std::make_unique<LevelState>());

	while (this->window.isOpen()) {
		sf::Event event = sf::Event();

		// Mouse Wheel events
		extern float HWheelDelta;
		extern float VWheelDelta;

		HWheelDelta = 0;
		VWheelDelta = 0;

		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				this->window.close();
				break;
			}

			if (event.type == sf::Event::MouseWheelScrolled) {
				if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
					VWheelDelta = event.mouseWheelScroll.delta;

				else if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
					HWheelDelta = event.mouseWheelScroll.delta;
			}

		}

		window.clear(sf::Color::Black);
		sf::Time timeElapsed = clock.restart();

		sm.update(timeElapsed);

		window.display();
		entityManager.cleanup();

	}

	return 0;
}
