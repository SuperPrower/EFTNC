#ifndef EFTNC_GAME_HPP
#define EFTNC_GAME_HPP

/// Game class handles game logic, loop, elapsed time, make calls to State Manager.

#include "resource_managers/GraphicResourcesManager.hpp"
#include "resource_managers/AudioResourcesManager.hpp"

#include "states/StateManager.hpp"
#include "EntityManager.hpp"
#include "GroupManager.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Game {
public:
	Game();

	/**
	 * Game loop runs until game is closed or exit conditions are met.
	 * It cycles trough getting events, updating everything and rendering.
	 * @return 0 on success
	 */
	int gameLoop();

private:
	sf::RenderWindow window;
	sf::Clock clock;

	SystemManager systemManager;
	EntityManager entityManager;
	GroupManager groupManager;

	GraphicResourcesManager m_graphicsMan;
	AudioResourcesManager m_audioMan;
};

#endif //EFTNC_GAME_HPP
