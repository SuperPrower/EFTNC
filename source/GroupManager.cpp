#include "GroupManager.hpp"

#include <iostream>
#include <utility>

GroupManager::GroupManager(std::size_t maxEntities, std::size_t maxGroups) {
	matrix = new bool *[maxEntities];
	for (std::size_t en_index = 0; en_index < maxEntities; en_index++)
		matrix[en_index] = new bool[maxGroups];

	max_entities = maxEntities;
	max_groups = maxGroups;
}

GroupManager::~GroupManager() {
	for (std::size_t i = 0; i < max_entities; i++)
		delete[] matrix[i];
	delete[] matrix;
}

void GroupManager::addEntity(eftnc::entity_t entity) {
	// Resize only in entities dimension if there are too many entities
	if (entities.size() == max_entities)
		resize(2 * max_entities, max_groups);

	std::size_t entity_index = entities.size();
	entities.insert(std::make_pair(entity, entity_index));

	// Exclude new entity from all existing groups
	for (std::size_t gr = 0; gr < groups.size(); gr++)
		matrix[entity_index][gr] = false;
}

void GroupManager::addGroup(std::string group) {
	// Resize only in groups dimension if there are too many groups
	if (groups.size() == max_groups)
		resize(max_entities, 2 * max_groups);

	std::size_t group_index = groups.size();
	groups.insert(std::make_pair(group, group_index));

	// Exclude all existing entities from new group
	for (std::size_t en = 0; en < entities.size(); en++)
		matrix[en][group_index] = false;
}

void GroupManager::putIntoGroups(eftnc::entity_t entity, std::string groups) {
	// Entity index in matrix
	std::size_t en_index;
	// Groups indices in matrix
	std::vector<std::size_t> gr_indices;

	if (not (parseEntity(entity, en_index) and parseGroups(groups, gr_indices)))
		return;

	for (auto group_index : gr_indices) {
		matrix[en_index][group_index] = true;
	}
}

void GroupManager::removeFromGroups(eftnc::entity_t entity, std::string groups) {
	// Entity index in matrix
	std::size_t en_index;
	// Groups indices in matrix
	std::vector<std::size_t> gr_indices;

	if (not (parseEntity(entity, en_index) and parseGroups(groups, gr_indices)))
		return;

	for (auto group_index : gr_indices) {
		matrix[en_index][group_index] = false;
	}
}

bool GroupManager::hasGroups(eftnc::entity_t entity, std::string groups) {
	// Entity index in matrix
	std::size_t en_index;
	// Groups indices in matrix
	std::vector<std::size_t> gr_indices;

	if (not (parseEntity(entity, en_index) and parseGroups(groups, gr_indices)))
		throw eftnc::generic_exception("Error while parsing entities of groups");

	// Assume the entity is in all groups (intersection)
	bool inGroup = true;
	// And try to falsify the assumption
	for (auto iter = gr_indices.begin(); iter != gr_indices.end() and inGroup; iter++) {
		inGroup = matrix[en_index][*iter];
	}

	return inGroup;
}

void GroupManager::getEntities(std::string groups, std::vector<eftnc::entity_t> &result) {
	// Groups indices in matrix
	std::vector<std::size_t> gr_indices;

	if (not parseGroups(groups, gr_indices))
		throw eftnc::generic_exception("Groups parse error");

	for (auto &entity : entities) {
		auto en_index = entity.second;
		// Check if the entity belongs to all specified groups
		bool add = true;
		for (auto gr_index : gr_indices)
			add = matrix[en_index][gr_index];

		// If so -- add it to the resulting list
		if (add)
			result.push_back(entity.first);
	}
}

void GroupManager::removeEntity(eftnc::entity_t entity) {
	// Index of the entity to delete
	std::size_t en_index;
	try {
		en_index = entities.at(entity);
	}
	catch(std::out_of_range &e) {
		std::cerr << e.what() << '\n';
		return;
	}

	// Replace the data of the removed entity with the data
	// of the positionally last entity in matrix
	std::size_t last_entity_index = entities.size() - 1;
	for (std::size_t gr_index = 0; gr_index < groups.size(); gr_index++)
		matrix[en_index][gr_index] = matrix[last_entity_index][gr_index];

	// Delete the entity from the mapping
	entities.erase(entity);

	// Update the moved entity id -> index mapping
	for (auto &entity : entities) {
		if (entity.second == last_entity_index) {
			entity.second = en_index;
			break;
		}
	}
}

void GroupManager::removeGroup(std::string group) {
	// Index of the group to delete
	std::size_t gr_index;
	try {
		gr_index = groups.at(group);
	}
	catch (std::out_of_range &e) {
		std::cerr << e.what() << '\n';
		return;
	}

	// Replace the data of the removed group with the data
	// of the positionally last group in matrix
	std::size_t last_group_index = groups.size() - 1;
	for (std::size_t en_index = 0; en_index < entities.size(); en_index++)
		matrix[en_index][gr_index] = matrix[en_index][last_group_index];

	// Delete the group from the mapping
	groups.erase(group);

	// Update the moved group id -> index mapping
	for (auto &group : groups) {
		if (group.second == last_group_index) {
			group.second = gr_index;
			break;
		}
	}
}

void GroupManager::resize(std::size_t entities, std::size_t groups) {
	bool **temp = matrix;

	// Allocate new storage space
	matrix = new bool *[entities];
	for (std::size_t i = 0; i < entities; i++)
		matrix[i] = new bool[groups];

	// Copy existing data
	for (std::size_t en_index = 0; en_index < std::min(entities, max_entities); en_index++)
		for (std::size_t gr_index = 0; gr_index < std::min(groups, max_groups); gr_index++)
			matrix[en_index][gr_index] = temp[en_index][gr_index];

	// Deallocate old storage space
	for (std::size_t i = 0; i < max_entities; i++)
		delete[] temp[i];
	delete[] temp;

	// Update boundaries
	max_entities = entities;
	max_groups = groups;
}

bool GroupManager::parseEntity(eftnc::entity_t entity, std::size_t &en_index) {
	try {
		// find entity index
		en_index = entities.at(entity);
	}
	catch (std::out_of_range &e) {
		std::cerr << e.what();
		return false;
	}

	return true;
}

bool GroupManager::parseGroups(std::string groups, std::vector<std::size_t> &gr_indices) {
	try {
		// find indices of all groups by delimiter <space>
		std::string delimiter = " ";
		// The position of previously found delimiter
		std::size_t base = 0;
		// The position of next found delimiter
		std::size_t cursor = 0;
		// Extract strings mapping to groups until end of string is met
		while (cursor < groups.length() && cursor != std::string::npos) {
			// Find next delimiter
			cursor = groups.find(delimiter, base);
			// Extract group identifier from string
			std::string group = groups.substr(base, cursor - base);

			// Add group index from mapping to the list of indices
			gr_indices.push_back(this->groups.at(group));

			// Move the base
			base = cursor + 1;
		}
	}
	catch (std::out_of_range &e) {
		std::cerr << e.what() << '\n';
		return false;
	}

	return true;
}
