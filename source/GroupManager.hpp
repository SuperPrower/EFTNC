#ifndef EFTNC_GROUPMANAGER_HPP
#define EFTNC_GROUPMANAGER_HPP

#include <vector>
#include <map>
#include <cstddef>
#include "Types.hpp"

class GroupManager {
public:
	GroupManager(std::size_t maxEntities, std::size_t maxGroups);

	~GroupManager();

	void addEntity(eftnc::entity_t entity);

	void addGroup(std::string group);

	void putIntoGroups(eftnc::entity_t entity, std::string groups);

	void removeFromGroups(eftnc::entity_t entity, std::string groups);

	bool hasGroups(eftnc::entity_t entity, std::string groups);

	void getEntities(std::string groups, std::vector<eftnc::entity_t> &result);

	void removeGroup(std::string group);

	void removeEntity(eftnc::entity_t entity);

private:
	void resize(std::size_t entities, std::size_t groups);

	bool parseEntity(eftnc::entity_t entity, std::size_t &en_index);

	bool parseGroups(std::string groups, std::vector<std::size_t> &gr_indices);

	std::size_t max_entities;
	std::size_t max_groups;

	std::map<eftnc::entity_t, std::size_t> entities;
	std::map<std::string, std::size_t> groups;

	bool **matrix;
};

#endif // EFTNC_GROUPMANAGER_HPP
