#include "resource_managers/GraphicResourcesManager.hpp"

// TODO: Maybe, rework this into class? Will be easier to serialize and deserialize required components.
// TODO: Separate Component Managers and Resource Managers?

/**
 * Service Locator provides easy access to all components managers available.
 */

struct ServiceLocator {
	std::unique_ptr<GraphicResourcesManager> animationsMan;

	ServiceLocator() :
			animationsMan(std::make_unique<GraphicResourcesManager>())
	{}
};

