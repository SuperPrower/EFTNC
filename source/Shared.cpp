#include "Shared.hpp"

sf::RenderTarget * SharedData::renderTo = nullptr;

sf::Window * SharedData::referenceWindow = nullptr;

EntityManager * SharedData::entityManager = nullptr;

SystemManager * SharedData::systemManager = nullptr;

GraphicResourcesManager * SharedData::graphicResourcesManager = nullptr;

GroupManager * SharedData::groupManager = nullptr;

AudioResourcesManager * SharedData::audioResourcesManager = nullptr;