//
// Created by daniil on 28.12.17.
//

#include "SystemManager.hpp"
#include "systems/AbstractSystem.hpp"

bool SystemManager::addSystem(std::string id, AbstractSystem *system) {
    if (systems.count(id) > 0)
        return false;

    graphChanged = true;
    systems[id] = system;
    dependencies[id] = std::vector<std::string>();
    return true;
}

bool SystemManager::hasSystem(std::string id) {
    return systems.count(id) > 0;
}

AbstractSystem *SystemManager::getSystem(std::string id) {
    if (hasSystem(id))
        return systems[id];
    return nullptr;
}

const std::vector<AbstractSystem *> SystemManager::getAllSystems() {
	if (cacheInvalid) {
		cache.clear();
		for (auto system : systems) {
			cache.push_back(system.second);
		}
	}
	return cache;
}

void SystemManager::addDependency(std::string dependant, std::string dependency) {
    dependencies[dependant].push_back(dependency);
    graphChanged = true;
}

void SystemManager::updateAll(sf::Time delta) {
    if (graphChanged) {
        // Order will contain the strictly ordered list of systems to run
        order.clear();

        std::map<std::string, std::vector<std::string>> dep_pool(dependencies);
        std::vector<std::string> free_nodes;

        // Create initial list of free nodes -- those without dependencies
        for (auto iter = dep_pool.begin(); iter != dep_pool.end();) {
            if (iter->second.empty()) {
                free_nodes.push_back(iter->first);
                iter = dep_pool.erase(iter);
            } else {
				++iter;
			}
        }

		// Loop until we have at least one node with resolved dependencies
        while (! free_nodes.empty()) {
            std::string id = free_nodes.back();
            free_nodes.pop_back();

            // Turn off the dependencies that were taken from free_nodes
			// To do this, walk through all non-free nodes to check if they've spared
            for (auto iter = dep_pool.begin(); iter != dep_pool.end();) {
				// Walk through the vector of dependencies and free the resolved ones
                for (auto er_iter = iter->second.begin(); er_iter != iter->second.end();) {
                    if (*er_iter == id) {
                        er_iter = iter->second.erase(er_iter);
                    } else {
						++er_iter;
					}
                }

                // A node may have resolved all its dependencies and is now free node
                if (iter->second.empty()) {
                    free_nodes.push_back(iter->first);
                    iter = dep_pool.erase(iter);
                } else {
					++iter;
				}
            }

            // The systems will be run from order.begin() till order.end()
            order.push_back(id);
        }

        // We may have circular dependency.
        // In this case the dependency pool will not be empty
        // because we couldn't resolve all the dependencies of some nodes
        if (! dep_pool.empty()) {
            throw eftnc::generic_exception("Circular dependency in systems graph");
        }
        graphChanged = false;
    }

    // Run all systems in order
    // If new dependencies or systems haven't been added since last time
    // we can use order constructed previously
    for (auto &iter : order) {
        systems[iter]->update(delta);
    }
}
