//
// Created by daniil on 28.12.17.
//

#ifndef EFTNC_SYSTEMMANAGER_HPP
#define EFTNC_SYSTEMMANAGER_HPP


// TODO: migrate std::map into two std::vector

class AbstractSystem;

#include "Types.hpp"
#include <string>
#include <map>
#include <vector>
#include <SFML/System/Time.hpp>

class SystemManager {
public:
	SystemManager() = default;

	bool addSystem(std::string id, AbstractSystem * system);

	bool hasSystem(std::string id);

	AbstractSystem * getSystem(std::string id);

	const std::vector<AbstractSystem *> getAllSystems();

	void addDependency(std::string dependant, std::string dependency);

	void updateAll(sf::Time delta);

private:
	std::map<std::string, AbstractSystem *> systems;

	std::map<std::string, std::vector<std::string>> dependencies;

	std::vector<std::string> order;

	// This vector will be used to quickly delete entity from all systems
	std::vector<AbstractSystem *> cache;

	bool graphChanged = true;

	bool cacheInvalid = true;
};

#endif //EFTNC_SYSTEMMANAGER_HPP
