#ifndef EFTNC_TYPES_H
#define EFTNC_TYPES_H

#include <cstdint>
#include <string>

namespace eftnc {
	using entity_t = uint32_t;

	// Future-proofing. We might potentially decide to switch from numbers to objects
	const entity_t invalid_entity = 0;
	const entity_t first_entity = 1;

	class generic_exception : public std::exception {
	public:
		explicit generic_exception(std::string message) : msg(message) {}

		const char * what() const noexcept override { return msg.c_str(); }

	private:
		std::string msg;
	};
}

#endif //EFTNC_TYPES_H
