#ifndef EFTNC_COLLISIONCOMPONENT_HPP
#define EFTNC_COLLISIONCOMPONENT_HPP

#include <vector>
#include <Types.hpp>

struct CollisionComponent {
    char type; // Circle = 0, Rectangle = 1

    //*** THIS POINTS SHOULD BE OFFSET FROM POSITION COMPONENT ***
    // For Polygon there vector of (x,y) points where last point connected with the first;

    // For circle the first point radius stored at position x in (x,y);

    //*** THIS POINTS SHOULD BE OFFSET FROM POSITION COMPONENT ***
    // For Rectangle the min (x,y) point stored first and max (x,y) point stored second.
    std::vector<std::pair<float, float>> points;
};


#endif // EFTNC_COLLISIONCOMPONENT_HPP
