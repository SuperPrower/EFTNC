#ifndef EFTNC_GRAPHICS_COMPONENT_HPP
#define EFTNC_GRAPHICS_COMPONENT_HPP

#include "Types.hpp"

#include <SFML/System/Time.hpp>

struct GraphicsComponent {
	sf::Time 		animation_time_elapsed;
	eftnc::entity_t entity_index;
	uint64_t 		animation_index;
	uint64_t 		sprite_index;
	uint16_t 		frame_number;
	uint8_t 		flags;
	// TODO : Compress this, maybe? We will rarely need more than 8 layers, we could take 3 bits off flags.
	uint8_t 		z_level;
	float			scaleX;
	float 			scaleY;
};

#endif //EFTNC_GRAPHICS_COMPONENT_HPP
