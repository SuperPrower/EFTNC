//
// Created by daniil on 27.10.17.
//

#ifndef EFTNC_POSITIONCOMPONENT_HPP
#define EFTNC_POSITIONCOMPONENT_HPP

#include <Types.hpp>

/**
 * Position component contains the geometrical position and rotation
 * of the entity.
 */

struct PositionComponent {
	float x;                    // X-coordinate of the entity
	float y;                    // Y-coordinate of the entity
	float rotation;             // Angle of rotation, in degrees
};

#endif //EFTNC_POSITIONCOMPONENT_HPP
