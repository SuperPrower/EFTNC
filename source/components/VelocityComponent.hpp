#ifndef EFTNC_VELOCITYCOMPONENT_HPP
#define EFTNC_VELOCITYCOMPONENT_HPP

#include <Types.hpp>

struct VelocityComponent {
	float x_vel;	// X-axis velocity
	float y_vel;	// Y-axis velocity
};

#endif //EFTNC_VELOCITYCOMPONENT_HPP
