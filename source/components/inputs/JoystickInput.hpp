#ifndef EFTNC_JOYSTICK_INPUT_HPP
#define EFTNC_JOYSTICK_INPUT_HPP

#include "components/inputs/input.hpp"

#include <SFML/Window/Joystick.hpp>
#include <cmath>

class JoystickButtonInput : public Input {
public:
	explicit JoystickButtonInput(uint8_t joyNo, uint8_t button)
			: Input(eftnc::input::InputType::JoystickButton)
			, joyNo(joyNo)
			, button(button)
			, isPressed(false)
	{}

	void update() override {
		isPressed = sf::Joystick::isButtonPressed(joyNo, button);
	}

	void * getState() override {
		return static_cast<void *>(&isPressed);
	}

protected:
	uint8_t 	joyNo;
	uint8_t 	button;
	bool 		isPressed;
};

class JoystickAxisInput : public Input {
public:
	JoystickAxisInput(uint8_t joyNo, sf::Joystick::Axis axis, bool negative)
		: Input(eftnc::input::InputType::JoystickAxis)
		, joyNo(joyNo)
		, axis(axis)
		, isNegative(negative)
		, value(0.f)
	{}

	void update() override {
		float val = sf::Joystick::getAxisPosition(joyNo, axis);
		if ((isNegative && val < 0) || ((not isNegative) && val > 0)) {
			value = std::abs(val);
		} else {
			value = 0.f;
		}
	}

	void * getState() override {
		return static_cast<void *>(&value);
	}

protected:
	uint8_t 			joyNo;
	sf::Joystick::Axis 	axis;
	/* To separate moving axis up or down,
	 * we mark axis as, for example, X+ or X-,
	 * and ignore values of other sign.
	 */
	bool				isNegative;
	float				value;

};

#endif //EFTNC_JOYSTICK_INPUT_HPP
