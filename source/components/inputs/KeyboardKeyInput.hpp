#ifndef EFTNC_KEYBOARD_INPUT_HPP
#define EFTNC_KEYBOARD_INPUT_HPP

#include "components/inputs/input.hpp"

#include <SFML/Window/Keyboard.hpp>

class KeyboardKeyInput : public Input {
public:
	explicit KeyboardKeyInput(sf::Keyboard::Key key)
			: Input(eftnc::input::InputType::KeyboardKey)
			, key(key)
			, isPressed(false)
	{}

	void update() override {
		isPressed = sf::Keyboard::isKeyPressed(this->key);
	}

	void * getState() override {
		return static_cast<void *>(&isPressed);
	}

private:
	sf::Keyboard::Key 	key;
	bool 				isPressed;
};

#endif //EFTNC_KEYBOARD_INPUT_HPP
