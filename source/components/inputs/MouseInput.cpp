#include <components/inputs/MouseInput.hpp>

#include <cmath>
#include <Shared.hpp>

/** MOUSE BUTTONS INPUT **/

MouseButtonInput::MouseButtonInput(sf::Mouse::Button button)
	: Input(eftnc::input::InputType::MouseButton)
	, button(button)
	, isPressed(false)
{}

void MouseButtonInput::update() {
	isPressed = sf::Mouse::isButtonPressed(button);
}

void * MouseButtonInput::getState() {
	return static_cast<void *>(&isPressed);
}


/** MOUSE WHEEL INPUT **/

/*
 * Because Wheel state can't be queried in real-time,
 * we will use events to change those values while polling events,
 * and fetch them when required
 */
float HWheelDelta;
float VWheelDelta;

MouseWheelInput::MouseWheelInput(sf::Mouse::Wheel wheel, bool isNegative)
	: Input(eftnc::input::InputType::MouseWheel)
	, wheel(wheel)
	, isNegative(isNegative)
	, delta(0)
{}

void MouseWheelInput::update() {
	float delta = 0.f;
	switch (this -> wheel) {
		case sf::Mouse::VerticalWheel:
			delta = VWheelDelta;
			break;

		case sf::Mouse::HorizontalWheel:
			delta = HWheelDelta;
			break;
	}

	if ((isNegative && delta < 0) || ((not isNegative) && delta > 0)) {
		this->delta = std::abs(delta);
	} else {
		this->delta = 0.f;
	}

}

	void * MouseWheelInput::getState() {
		return static_cast<void *>(&delta);
	}


/** MOUSE POSITION INPUT **/

MousePointerInput::MousePointerInput()
	: Input(eftnc::input::InputType::MousePos)
	, position(0, 0)
{}

void MousePointerInput::update() {
	position = sf::Mouse::getPosition(*SharedData::referenceWindow);
}

void * MousePointerInput::getState() {
	return static_cast<void *>(&position);
}