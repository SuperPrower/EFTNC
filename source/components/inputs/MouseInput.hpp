#ifndef EFTNC_MOUSE_INPUT_HPP
#define EFTNC_MOUSE_INPUT_HPP

#include "components/inputs/input.hpp"

#include <SFML/Window/Mouse.hpp>
#include <cmath>

class MouseButtonInput : public Input {
public:
	explicit MouseButtonInput(sf::Mouse::Button button);

	void update() override;
	void * getState() override;

protected:
	sf::Mouse::Button	button;
	bool 				isPressed;
};


class MouseWheelInput : public Input {
public:
	explicit MouseWheelInput(sf::Mouse::Wheel wheel, bool isNegative);

	void update() override;
	void * getState() override;

protected:
	sf::Mouse::Wheel 	wheel;
	bool 				isNegative;
	float 				delta;
};

class MousePointerInput : public Input {
public:
	explicit MousePointerInput();

	void update() override;
	void * getState() override;

protected:
	sf::Vector2i		position;

};

#endif //EFTNC_MOUSE_INPUT_HPP
