#ifndef EFTNC_INPUT_HPP
#define EFTNC_INPUT_HPP

namespace eftnc::input {
	enum InputType {
		KeyboardKey,
		JoystickButton,
		JoystickAxis,
		MouseButton,
		MouseWheel,
		MousePos
	};

	/** All possible in-game actions **/
	enum Action {
		UP,
		DOWN,
		LEFT,
		RIGHT,
		FIRE1,
		AIM,
		AIM_X_POS,
		AIM_X_NEG,
		AIM_Y_POS,
		AIM_Y_NEG
	};

};

/**
 * Abstract class that represents one input type,
 * such as keyboard button or joystick axis
 */
class Input {
public:
	Input(eftnc::input::InputType t) : inputType(t) {}
	/**
	 * Check required input state
	 */
	virtual void update() = 0;

	/**
	 * Get state of Input
	 * @return universal pointer to state of input
	 * - we know how to handle it because we know type
	 */
	virtual void * getState() = 0;

	eftnc::input::InputType & getType() { return this->inputType; }

protected:
	eftnc::input::InputType inputType;

};


#endif //EFTNC_INPUT_HPP
