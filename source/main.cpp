#include "Game.hpp"
#include "Shared.hpp"

int main() {
	Game * game = new Game();
	int res = game->gameLoop();
	delete (game);
	return res;
}