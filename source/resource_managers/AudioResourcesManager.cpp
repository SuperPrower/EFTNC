#include "AudioResourcesManager.hpp"

#include <iostream>

void AudioResourcesManager::loadMusic(const std::string & filename) {
	std::string fullpath = mus_path + filename;
	this->currentMusic.openFromFile(fullpath);
}

void AudioResourcesManager::playMusic() {
	this->currentMusic.play();
}

void AudioResourcesManager::pauseMusic() {
	this->currentMusic.pause();
}

void AudioResourcesManager::stopMusic() {
	this->currentMusic.stop();
}

void AudioResourcesManager::loadSound(const std::string &filename, const std::string &soundname) {
	sf::SoundBuffer sb;
	std::string fullpath = snd_path + filename;

	if (sb.loadFromFile(fullpath)) {
		if (soundname == "") {
			this->soundSources[filename] = sb;
		} else {
			this->soundSources[soundname] = sb;
		}
	}
}

void AudioResourcesManager::deleteSound(const std::string &soundname) {
	this->soundSources.erase(soundname);
}


void AudioResourcesManager::playSound(const std::string &soundname, bool isRelative) {
	auto sbIt = this->soundSources.find(soundname);

	/* if such sound exists */
	if (sbIt != this->soundSources.end()) {
		auto isStopped = [](sf::Sound & s) { return s.getStatus() == sf::SoundSource::Status::Stopped; };
		auto sIt = std::find_if(this->playingSounds.begin(), this->playingSounds.end(), isStopped);

		// if debug
		// std::cout << "playing sound " << soundname << std::endl;

		// if no free sounds, create new one
		if (sIt == this->playingSounds.end()) {
			this->playingSounds.emplace_back();
		}

		sIt = this->playingSounds.begin();
		// and play sound
		sIt->setBuffer(sbIt->second);
		sIt->setRelativeToListener(isRelative);
		sIt->play();

		// remove remaining unused sounds
		this->playingSounds.erase(
				std::remove_if(this->playingSounds.begin(), this->playingSounds.end(), isStopped),
				this->playingSounds.end()
		);

	} else {
		std::cerr << "No sound " << soundname << " is loaded" << std::endl;
	}
}

void AudioResourcesManager::pauseSounds() {
	for (auto & sound : this->playingSounds) {
		sound.pause();
	}
}

void AudioResourcesManager::resumeSounds() {
	for (auto & sound : this->playingSounds) {
		if (sound.getStatus() == sf::SoundSource::Status::Paused) {
			sound.play();
		}
	}
}

void AudioResourcesManager::stopSounds(bool release) {
	if (release) {
		this->playingSounds.clear();
	} else {
		for (auto & sound : this->playingSounds) {
			sound.stop();
		}
	}
}
