#ifndef EFTNC_AUDIO_RESOURCES_MANAGER_HPP
#define EFTNC_AUDIO_RESOURCES_MANAGER_HPP

#include <SFML/Audio.hpp>

#include <map>
#include <vector>

/**
 * Audio Resources Manager allows pre-loading sounds,
 * playing them at request, playing music, stopping all sounds,
 * stopping, pausing music, etc.
 */
class AudioResourcesManager {
public:
	/* BGM functions */
	/**
	 * Assign file on disk to music source
	 *
	 * @param filename - music file name
	 */
	void loadMusic(const std::string & filename);
	/**
	 * Play current music track
	 *
	 * @throws std::runtime_error if music file not loaded or found
	 */
	void playMusic();
	/**
	 * Pause current music track
	 */
	void pauseMusic();
	/**
	 * Stop current music track
	 */
	void stopMusic();

	/* Sound methods */

	/**
	 * Load sound from disk to sound buffer collection
	 *
	 * @param filename - name of file to load from disk.
	 * @param soundname - name to map soundbuffer to
	 */
	void loadSound(const std::string & filename, const std::string & soundname = "");

	/**
	 * Release sound from sound buffers collection
	 *
	 * @param soundname - name of soundbuffer to release
	 */
	void deleteSound(const std::string & soundname);

	/**
	 * Allocate new sf::Sound, assign sound buffer to it and play
	 *
	 * @param soundname - sound name to play
	 */
	void playSound(const std::string & soundname, bool isRelative = false);

	/** Pause all sounds */
	void pauseSounds();

	/** Resume all paused sounds */
	void resumeSounds();

	/**
	 * Stop all sounds and release all sound buffers
	 *
	 * @param release - release sf::Sounds or leave them for future
	 */
	void stopSounds(bool release = true);

private:
	std::string snd_path = "resources/sounds/";
	std::string mus_path = "resources/music/";

	/** sound comparator for re-using and fast sorting sounds */
	struct SoundComparator {
		bool operator()(const sf::Sound &sound_l, const sf::Sound &sound_r) {
			return sound_l.getStatus() < sound_r.getStatus();
		}
	};

	SoundComparator comparator;

	/**
	 * Since Sounds are just micro-players for SoundBuffers,
	 * would be silly to have as many sounds as buffers,
	 * especially considering that we can have many
	 * identical sounds playing at the same time.
	 *
	 * So, Sounds are being created on request, and
	 * deleted (or reused) whenever they have finished playing
	 * their SoundBuffer content.
	 */
	std::vector<sf::Sound> playingSounds;

	/**
	 * SoundBuffer storage.
	 */
	std::map<std::string, sf::SoundBuffer> soundSources;

	/**
	 * Current BGM. Not necessary being played.
	 */
	sf::Music currentMusic;


};


#endif //EFTNC_AUDIO_RESOURCES_MANAGER_HPP
