#include "LevelState.hpp"

#include <systems/PositionSystem.hpp>
#include <systems/CollisionSystem.hpp>
#include "Shared.hpp"

void LevelState::atStart() {
	if (SharedData::graphicResourcesManager->loadJSON("test.json"))
		throw eftnc::generic_exception("Unable to load test.json");
	SharedData::graphicResourcesManager->parseAnimations();
	entity_1 = SharedData::entityManager->createFromSchema("test_square.json");
	entity_2 = SharedData::entityManager->createFromSchema("evil_square.json");

	SharedData::audioResourcesManager->loadSound("gunshot.ogx", "gunshot");
}

void LevelState::atFinish() {
	// Nothing to do
}

void LevelState::update(sf::Time dt) {
	SharedData::systemManager->updateAll(dt);
}
