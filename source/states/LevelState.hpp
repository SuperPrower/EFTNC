#ifndef EFTNC_LEVEL_STATE_HPP
#define EFTNC_LEVEL_STATE_HPP

/// Right now this state is created for test needs. Later it will be main gameplay state.

#include <systems/ControlsSystem.hpp>
#include "GameState.hpp"
#include "resource_managers/GraphicResourcesManager.hpp"

class LevelState : public GameState {
public:
	void atStart() override;

	void atFinish() override;

	void update(sf::Time dt) override;

private:
	eftnc::entity_t entity_1;
	eftnc::entity_t entity_2;
};


#endif //EFTNC_LEVEL_STATE_HPP
