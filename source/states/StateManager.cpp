#include "StateManager.hpp"

StateManager::StateManager() {}

void StateManager::pushState(std::unique_ptr<GameState> s) {
	this->states.push_back(std::move(s));
	this->states.back()->atStart();
}

void StateManager::changeState(std::unique_ptr<GameState> s) {
	this->popState();
	this->pushState(std::move(s));
}

void StateManager::popState() {
	if (!states.empty()) {
		states.back()->atFinish();
		states.pop_back();
	}
}

void StateManager::update(sf::Time dt) {
	this->states.back()->update(dt);
}
