#ifndef EFTNC_ABSTRACTSYSTEM_HPP
#define EFTNC_ABSTRACTSYSTEM_HPP

#include <SFML/System/Time.hpp>
#include <SystemManager.hpp>
#include <json/json.hpp>

class AbstractSystem {
public:
	/**
	 * @brief This method initializes the system and must be called
	 * right after its creation.
	 *
	 * @param systemManager Global system manager.
	 */
	virtual void onCreate(SystemManager * systemManager) = 0;

	/**
	 * @brief Update the system state.
	 *
	 * @param delta time elapsed since previous update.
	 */
	virtual void update(sf::Time delta) = 0;

	/**
	 * @brief Assemble standard system's component if possible.
	 *
	 * @throws eftnc::generic_exception if the creation of standard component is not possible.
	 *
	 * @param entity Entity ID.
	 */
	virtual void assemble(eftnc::entity_t entity) = 0;

	/**
	 * @brief Construct system's corresponding component from JSON string.
	 *
	 * @param entity Entity ID.
	 * @param schema JSON string schema.
	 */
	virtual void assemble(eftnc::entity_t entity, nlohmann::json schema) = 0;

	/**
	 * @brief Tells if the system contains the component for the specified entity.
	 *
	 * @param entity Entity ID.
	 *
	 * @return True if the system contains corresponding component for the given entity, false otherwise
	 */
	virtual bool hasComponent(eftnc::entity_t entity) = 0;

	/**
	 * @brief Deletes the component from the system, if it is present there.
	 *
	 * @param entity Entity ID.
	 */
	virtual void deleteComponent(eftnc::entity_t entity) = 0;

	/**
	 * @brief This method deinitializes the system and must be called just before system deletion.
	 */
	virtual void onDestroy() = 0;

protected:
	AbstractSystem() = default;
};

#endif //EFTNC_ABSTRACTSYSTEM_HPP
