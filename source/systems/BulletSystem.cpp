#include "BulletSystem.hpp"
#include "CollisionSystem.hpp"
#include <Shared.hpp>

const std::string BulletSystem::bulletID = "bullet";

void BulletSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
	SharedData::groupManager->addGroup("bullet");
	SharedData::groupManager->addGroup("vulnerable");
}

const std::string &BulletSystem::getID() {
	return bulletID;
}

void BulletSystem::update(sf::Time delta) {
	std::vector<eftnc::entity_t> bullets;
	std::vector<eftnc::entity_t> vulnerables;

	SharedData::groupManager->getEntities("bullet", bullets);
	SharedData::groupManager->getEntities("vulnerable", vulnerables);

	CollisionSystem * cs = dynamic_cast<CollisionSystem *>
			(SharedData::systemManager->getSystem(CollisionSystem::getID()));

	for (auto bullet : bullets) {
		for (auto vulnerable : vulnerables) {
			if (cs->hasIntersection(bullet, vulnerable)) {
				// Insert the code of collsion handling here
				SharedData::entityManager->deleteEntity(bullet);
				// One bullet may have only one collision
				break;
			}
		}
	}
}

void BulletSystem::assemble(eftnc::entity_t entity) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, "vulnerable");
}

void BulletSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, schema.value("groups", "vulnerable"));
}

bool BulletSystem::hasComponent(eftnc::entity_t entity) {
	return SharedData::groupManager->hasGroups(entity, "vulnerable")
			or SharedData::groupManager->hasGroups(entity, "bullet");
}

void BulletSystem::deleteComponent(eftnc::entity_t entity) {
	SharedData::groupManager->removeFromGroups(entity, "bullet vulnerable");
}

void BulletSystem::onDestroy() {
	SharedData::groupManager->removeGroup("bullet");
	SharedData::groupManager->removeGroup("vulnerable");
}
