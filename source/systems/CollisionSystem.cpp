#include <components/CollisionComponent.hpp>
#include <Shared.hpp>
#define TINYC2_IMPLEMENTATION
#include <collision/tinyc2.h>
#include "CollisionSystem.hpp"
#include "PositionSystem.hpp"
#include "VelocitySystem.hpp"

const std::string CollisionSystem::collisionID = "collision";

const std::string &CollisionSystem::getID() {
    return collisionID;
}

void CollisionSystem::onCreate(SystemManager *systemManager) {
    systemManager->addSystem(getID(), this);
    // Collision must execute AFTER Velocity
    systemManager->addDependency(getID(), VelocitySystem::getID());
    // Collision must execute BEFORE Position
    systemManager->addDependency(PositionSystem::getID(), getID());
}

void CollisionSystem::update(sf::Time delta) {
    // Nothing to do
}

void CollisionSystem::assemble(eftnc::entity_t entity) {
    throw eftnc::generic_exception("Standard assemble in collision system is not supported");
}

void CollisionSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
    CollisionComponent component;

    component.type = schema.at("type").get<char>();

    // points: [ [1,1] [1,1] [1,1] ]
    for (auto &coord : schema) {
        component.points.push_back(std::make_pair(coord[0].get<float>(),
                coord[1].get<float>()));
    }

    components.insert(std::make_pair(entity, component));
}

bool CollisionSystem::hasComponent(eftnc::entity_t entity) {
    return components.count(entity) > 0;
}

CollisionComponent &CollisionSystem::getComponent(eftnc::entity_t entity) {
    auto iter = components.find(entity);
    if (iter == components.end())
        throw eftnc::generic_exception("No such component in collision system");
    return const_cast<CollisionComponent &> (iter->second);
}

void CollisionSystem::deleteComponent(eftnc::entity_t entity) {
    if (components.erase(entity) <= 0)
        throw eftnc::generic_exception("Tried to erase component not present in system");
}

bool CollisionSystem::hasIntersection(eftnc::entity_t entity_one, eftnc::entity_t entity_two) {
    CollisionComponent &component_one = getComponent(entity_one);
    CollisionComponent &component_two = getComponent(entity_two);

    // Take position of both entities
    PositionSystem * position_system = dynamic_cast<PositionSystem *>
            (SharedData::systemManager->getSystem(PositionSystem::getID()));

    const PositionComponent &position_one = position_system->getComponent(entity_one);
    const PositionComponent &position_two = position_system->getComponent(entity_two);

    void * first_hitbox = nullptr, * second_hitbox = nullptr;
    C2_TYPE first_type, second_type;

    if (component_one.type == 0) {
        c2Circle * circle = new c2Circle;
        circle->p = c2V(position_one.x, position_one.y);
        circle->r = component_one.points[0].first;
        first_hitbox = circle;
        first_type = C2_CIRCLE;
    }
    else if (component_one.type == 1) {
        c2AABB * aabb = new c2AABB;
        aabb->min = c2V(position_one.x + component_one.points[0].first,
                position_one.y + component_one.points[0].second);
        aabb->max = c2V(position_one.x + component_one.points[1].first,
                position_one.y + component_one.points[1].second);
        first_hitbox = aabb;
        first_type = C2_AABB;
    }
    else {
        throw eftnc::generic_exception("Unknown type for collision checking");
    }

    if (component_two.type == 0) {
        c2Circle * circle = new c2Circle;
        circle->p = c2V(position_two.x, position_two.y);
        circle->r = component_two.points[0].first;
        second_hitbox = circle;
        second_type = C2_CIRCLE;
    }
    else if (component_two.type == 1) {
        c2AABB * aabb = new c2AABB;
        aabb->min = c2V(position_two.x + component_two.points[0].first,
                position_two.y + component_two.points[0].second);
        aabb->max = c2V(position_two.x + component_two.points[1].first,
                position_two.y + component_two.points[1].second);
        second_hitbox = aabb;
        second_type = C2_AABB;
    }
    else {
        throw eftnc::generic_exception("Unknown type for collision checking");
    }

    return (bool) c2Collided(first_hitbox, nullptr, first_type,
            second_hitbox, nullptr, second_type);
}


std::pair<float, float> CollisionSystem::buildIntersectionManifold(eftnc::entity_t entity_one, eftnc::entity_t entity_two) {
    CollisionComponent &component_one = getComponent(entity_one);
    CollisionComponent &component_two = getComponent(entity_two);

    // Take position of both entities
    PositionSystem * position_system = dynamic_cast<PositionSystem *>
            (SharedData::systemManager->getSystem(PositionSystem::getID()));

    const PositionComponent &position_one = position_system->getComponent(entity_one);
    const PositionComponent &position_two = position_system->getComponent(entity_two);

    void * first_hitbox = nullptr, * second_hitbox = nullptr;
    C2_TYPE first_type, second_type;

    if (component_one.type == 0) {
        c2Circle * circle = new c2Circle;
        circle->p = c2V(position_one.x, position_one.y);
        circle->r = component_one.points[0].first;
        first_hitbox = circle;
        first_type = C2_CIRCLE;
    }
    else if (component_one.type == 1) {
        c2AABB * aabb = new c2AABB;
        aabb->min = c2V(position_one.x + component_one.points[0].first,
                position_one.y + component_one.points[0].second);
        aabb->max = c2V(position_one.x + component_one.points[1].first,
                position_one.y + component_one.points[1].second);
        first_hitbox = aabb;
        first_type = C2_AABB;
    }
    else {
        throw eftnc::generic_exception("Unknown type for collision checking");
    }

    if (component_two.type == 0) {
        c2Circle * circle = new c2Circle;
        circle->p = c2V(position_two.x, position_two.y);
        circle->r = component_two.points[0].first;
        second_hitbox = circle;
        second_type = C2_CIRCLE;
    }
    else if (component_two.type == 1) {
        c2AABB * aabb = new c2AABB;
        aabb->min = c2V(position_two.x + component_two.points[0].first,
                position_two.y + component_two.points[0].second);
        aabb->max = c2V(position_two.x + component_two.points[1].first,
                position_two.y + component_two.points[1].second);
        second_hitbox = aabb;
        second_type = C2_AABB;
    }
    else {
        throw eftnc::generic_exception("Unknown type for collision checking");
    }

    c2Manifold manifold;
    c2Collide(first_hitbox, nullptr, first_type,
            second_hitbox, nullptr, second_type,
            &manifold);

    std::pair<float, float> opposing_force_vector;

    if (manifold.count > 0) {
        // -1 multiplier is needed because the vector must have the opposite direction
        opposing_force_vector.first = (-1) * manifold.normal.x * manifold.depths[0];
        opposing_force_vector.second = (-1) * manifold.normal.y * manifold.depths[0];
    }
    else {
        throw eftnc::generic_exception("Entities does not collide");
    }

    return opposing_force_vector;
}

void CollisionSystem::onDestroy() {
	// Nothing to do
}
