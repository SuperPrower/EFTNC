#ifndef EFTNC_COLLISIONSYSTEM_HPP
#define EFTNC_COLLISIONSYSTEM_HPP

#include <components/PositionComponent.hpp>
#include <components/CollisionComponent.hpp>
#include <map>
#include "AbstractSystem.hpp"

class CollisionSystem : public AbstractSystem {
public:
    static const std::string &getID();

    void onCreate(SystemManager *systemManager) override;

    void update(sf::Time delta) override;

    void assemble(eftnc::entity_t entity) override;

    // JSON should contain type of figure with set of points.
    // points should be a JSON array
    //***************************************************************************
    // For Polygon there vector of (x,y) points where last point connected with the first.
    // THIS POINTS SHOULD BE DEFINED AS AN OFFSET FROM POSITION COMPONENT (X, Y);
    // EXAMPLE OF POLYGON IN FORM  __:
    //                           \_/ poisition is (10, 10)
    // lefttop point is (-2, -2), point for drawing (10,10) + (-2, -2) = (8, 8).
    // AND SO ON
    //****************************************************************************

    // For circle the first point radius stored at position x in (x,y);

    //****************************************************************************
    // For Rectangle the min (x,y) point stored first and max (x,y) point stored second.
    // THIS POINTS SHOULD BE DEFINED AS AN OFFSET FROM POSITION COMPONENT (X, Y);
    //****************************************************************************
    void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

    bool hasComponent(eftnc::entity_t entity) override;

    CollisionComponent &getComponent(eftnc::entity_t entity);

    void deleteComponent(eftnc::entity_t entity) override;

    void onDestroy() override;

    bool hasIntersection(eftnc::entity_t entity_one, eftnc::entity_t entity_two);

    std::pair<float, float> buildIntersectionManifold(eftnc::entity_t entity_first, eftnc::entity_t entity_two);

private:
    static const std::string collisionID;

    std::map<eftnc::entity_t, CollisionComponent> components;
};


#endif //EFTNC_COLLISIONSYSTEM_HPP
