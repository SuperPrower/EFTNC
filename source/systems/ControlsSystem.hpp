#ifndef EFTNC_CONTROLS_SYSTEM_HPP
#define EFTNC_CONTROLS_SYSTEM_HPP

#include "systems/AbstractSystem.hpp"
#include "components/ControlsComponent.hpp"

#include <vector>
#include <components/PositionComponent.hpp>

/**
 * Controls System is responsible for parsing pressed keys and
 * buttons and telling corresponding component about happened action (?)
 * It utilizes real-time fu
 */

class ControlsSystem : public AbstractSystem {
public:

	void onCreate(SystemManager * systemManager) override;

	void onDestroy() override;

	static const std::string &getID();

	void assemble(eftnc::entity_t entity) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	/**
	 * Creates a new controls component
	 *
	 * @param controls Control Scheme for entity
	 */
	void newComponent(struct ControlsComponent &);

	/**
	 * Updates all control components
	 */
	void update(sf::Time delta) override;

	/**
	 * Get reference to component with given entity id
	 *
	 * @param entity_id: Entity ID
	 */
	ControlsComponent &getComponent(eftnc::entity_t entity_id);

	/**
	 * Deletes the component for the given entity
	 *
	 * @param entity Entity ID
	 */
	void deleteComponent(eftnc::entity_t) override;

	bool hasComponent(eftnc::entity_t) override;


protected:
	std::vector<struct ControlsComponent> components;

	static const std::string controlsID;

private:
	/**
	 * Get Linear value, i.e. value from 0 to 1, from control,
	 * such as directional controls, acceleration, etc.
	 *
	 * Works for everything except for mouse position.
	 *
	 * @param input - input object
	 * @param dest - value which value needs to be changed
	 * @param multiplier - value to multiply input state (that is from 0 to 1) by
	 */
	template <typename T>
	void handleLinearControl(std::shared_ptr<Input> input, T & dest, float multiplier = 1.f);

	/**
	 * Handle Mouse Position
	 *
	 * Currently assuming that cursor is it's own entity with it's own position component.
	 * In future, just replace it with proper component with X and Y.
	 *
	 * TODO: Replace position component with proper Mouse Cursor Position Component
	 */
	// void handleCursorPosition(std::shared_ptr<Input> input, PositionComponent & pos);

	/**
	 * Handle Cursor Movement with buttons
	 * @param input
	 * @param pos
	 * @param delta
	 * @param speed
	 */
	// void handleCursorMovement(std::shared_ptr<Input> input, PositionComponent & pos, sf::Time delta, float speed);

	/**
	 * Aiming Angle is similar to cursor position, but it is not used to
	 * move the cursor, but rather to calculate crosshair angle relative to
	 * player position on the screen (in case of mouse)
	 *
	 * TODO: Pass proper Component and destination
	 */
	// void handleAimingAngle(std::shared_ptr<Input> input);

};


#endif //EFTNC_CONTROLS_SYSTEM_HPP
