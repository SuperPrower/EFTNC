#include "GraphicsSystem.hpp"
#include "Shared.hpp"

const std::string GraphicsSystem::graphicsID = "graphics";

struct graphicsHeap {
	bool operator()(const GraphicsComponent &first, const GraphicsComponent &second) {
		return first.z_level < second.z_level;
	}
};

const std::string &GraphicsSystem::getID() {
	return graphicsID;
}

void GraphicsSystem::assemble(eftnc::entity_t entity, nlohmann::json json) {
	// NOTE : The game is supposed to crash when missing something essential, like a texture file

	newComponent(entity,
				 json["file"].get<std::string>(),
				 json.value("animation", ""),
				 json.value("flags", (uint8_t) 1),
				 json.value("layer", 0),
				 json.value("scaleX", 1.f),
				 json.value("scaleY", 1.f)
	);
}

void GraphicsSystem::newComponent(const eftnc::entity_t &entity, const std::string &texture_name,
								  std::string animation_name,
								  uint8_t flags, const uint8_t &priority,
								  const float scaleX, const float scaleY) {
	GraphicsComponent added;

	added.entity_index = entity;
	added.animation_time_elapsed = sf::Time::Zero;
	added.sprite_index = graphicsManager->getTextureIndex(texture_name);

	if (animation_name.empty()) {
		AnimationResource anim;
		sf::Texture tex;
		sf::IntRect boundingBox;
		// NOTE : I'm not sure if the info about the texture is stored in memory or in GPU.
		// Let's assume the worst and copy the size just in case
		sf::Vector2u size;

		tex = SharedData::graphicResourcesManager->getTexture(added.sprite_index);
		size = tex.getSize();
		boundingBox.left = 0;
		boundingBox.top = 0;
		boundingBox.height = size.y;
		boundingBox.width = size.x;

		animation_name = texture_name;
		anim.name = texture_name;
		anim.frame = boundingBox;
		anim.frameTime = sf::Time::Zero;
		anim.loop = false;
		anim.nFrames = 1;
		SharedData::graphicResourcesManager->addAnimation(anim);
		if ((flags & GRAPHICS_STATIC) == 0)
			flags += GRAPHICS_STATIC;
	}

	added.animation_index = graphicsManager->getAnimationIndex(animation_name);

	added.frame_number = 0;

	added.flags = flags;
	added.z_level = priority;

	added.scaleX = scaleX;
	added.scaleY = scaleY;

	components.push_back(added);
	dirty = true;
}

void GraphicsSystem::update(sf::Time delta) {
	// Moved outside as to not create them every other time
	sf::Sprite sprite;
	AnimationResource animation;
	sf::IntRect rect;
	PositionComponent current_position;
	sf::Vector2f center;
	// Should be fast enough. According to cppreference.com, there should be be at most 3 * |components| comparisons.
	// Although there might be a problem with bullet-hellish enemies and bosses
	if (dirty) {
		graphicsHeap comparator;
		std::make_heap(components.rbegin(), components.rend(), comparator);
		dirty = false;
	}

	for (auto &component : components) {
		if (component.flags & GRAPHICS_VISIBLE) {
			// TODO : Potentially add a way to have multiple rows of animation
			animation = graphicsManager->getAnimation(component.animation_index);
			// TODO: Add position component existence check
			current_position = pos->getComponent(component.entity_index);
			center = sf::Vector2f((float) (animation.frame.left + (animation.frame.width / 2)),
								  (float) (animation.frame.top + (animation.frame.height / 2)));

			rect.left = (animation.frame.left + animation.frame.width) * component.frame_number;
			rect.width = animation.frame.width;
			rect.top = animation.frame.top;
			rect.height = animation.frame.height;

			sprite.setTexture(graphicsManager->getTexture(component.sprite_index));
			sprite.setTextureRect(rect);
			sprite.setPosition(sf::Vector2f(current_position.x + center.x, current_position.y + center.y));
			sprite.setRotation(current_position.rotation);
			sprite.setOrigin(center);
			sprite.setScale(component.scaleX, component.scaleY);

			SharedData::renderTo->draw(sprite);

			if (!(component.flags & GRAPHICS_STATIC)) {
				component.animation_time_elapsed += delta;
				while (component.animation_time_elapsed > animation.frameTime) {
					component.animation_time_elapsed -= animation.frameTime;

					if (component.frame_number < animation.nFrames - 1) {
						component.frame_number++;
					} else if (animation.loop) {
						component.frame_number = 0;
					}
				}
			}
		}
	}
}

/*
 * TODO : if deletion is slow/often needed, then make entities vector into an arrayed dense tree
 */
void GraphicsSystem::deleteComponent(eftnc::entity_t entity) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c] = *components.rbegin();
			components.pop_back();
			// TODO : What about keeping an index of boundaries for z-levels? E.g. priority 1 stops at 4, 2 at 6 etc. This way, we can just swap the last element of the boundary
			dirty = true;
			return;
		}
	}
}

bool GraphicsSystem::hasComponent(eftnc::entity_t entity) {
	for (auto &component : components) {
		if (component.entity_index == entity) {
			return true;
		}
	}

	return false;
}


/*
 * TODO : if searching is slow/often needed, then make entities vector into an arrayed dense tree
 */
void GraphicsSystem::toggleFlags(const eftnc::entity_t &entity, const uint8_t &flags) {
	for (auto &component : components) {
		if (component.entity_index == entity) {
			component.flags ^= flags;
			return;
		}
	}
}

void GraphicsSystem::setSprite(const eftnc::entity_t &entity, const std::string &name) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c].sprite_index = graphicsManager->getTextureIndex(name);
			components[c].frame_number = 0;
			components[c].animation_time_elapsed = sf::Time::Zero;
		}
	}
}

void GraphicsSystem::setAnimation(const eftnc::entity_t &entity, const std::string &name) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c].animation_index = graphicsManager->getAnimationIndex(name);
			components[c].frame_number = 0;
			components[c].animation_time_elapsed = sf::Time::Zero;
		}
	}
}

void GraphicsSystem::setScaleX(const eftnc::entity_t &entity, const float &scale) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c].scaleX = scale;
		}
	}
}

void GraphicsSystem::setScaleY(const eftnc::entity_t &entity, const float &scale) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c].scaleY = scale;
		}
	}
}

void GraphicsSystem::resetAnimation(const eftnc::entity_t &entity) {
	for (unsigned int c = 0; c < components.size(); ++c) {
		if (components[c].entity_index == entity) {
			components[c].frame_number = 0;
			components[c].animation_time_elapsed = sf::Time::Zero;
		}
	}
}