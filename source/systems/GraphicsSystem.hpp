#ifndef EFTNC_GRAPHICSSYSTEM_H
#define EFTNC_GRAPHICSSYSTEM_H

#include "Types.hpp"
#include "components/GraphicsComponent.hpp"

#include <resource_managers/GraphicResourcesManager.hpp>
#include <systems/PositionSystem.hpp>

#include <SFML/Graphics.hpp>

// Masks for component flags
const uint8_t GRAPHICS_VISIBLE = 1;
const uint8_t GRAPHICS_STATIC = 2;

/*
 *
 *   _______ ____  _____   ____
 *	|__   __/ __ \|  __ \ / __ \ _
 *     | | | |  | | |  | | |  | (_)
 *     | | | |  | | |  | | |  | |
 *     | | | |__| | |__| | |__| |_
 *     |_|  \____/|_____/ \____/(_)
 *
 * Refactor all of this shit. There is a lot of ad-hoc code, moved code and everything else.
 */
// The system itself
class GraphicsSystem : public AbstractSystem {
public:
	GraphicsSystem(GraphicResourcesManager * manager) {
		this->graphicsManager = manager;
	}

	void onCreate(SystemManager * systemManager) override {
		systemManager->addSystem(getID(), this);
		systemManager->addDependency(getID(), PositionSystem::getID());
		this->pos = dynamic_cast<PositionSystem *>(systemManager->getSystem("position"));
		// TODO: Add exceptional case handling
		dirty = false;
	};

	static const std::string &getID();

	void assemble(eftnc::entity_t entity, nlohmann::json json) override;

	void assemble(eftnc::entity_t entity) override {
		throw eftnc::generic_exception("Unable to default assemble a graphicsComponent");
	}

	/**
	 * Creates a new graphics component
	 *
	 * @param entity 			Entity ID
	 * @param texture_name 		Name of the required texture
	 * @param animation_name	Name of the required animation/bounding box
	 * @param flags				Initial flags for the component
	 * @param priority			On what layer should the graphics reside
	 * @param TODO: описать все параметры
	 */
	void
	newComponent(const eftnc::entity_t &,
				 const std::string &, std::string,
				 uint8_t, const uint8_t &,
				 const float, const float);

	/**
	 * Draws all the components
	 *
	 * @param target	Object to draw to
	 * @param delta		time between the current and the previous drawing
	 */
	void update(sf::Time delta) override;

	/**
	 * Deletes the graphics for the given entity
	 *
	 * @param entity Entity ID
	 */
	void deleteComponent(eftnc::entity_t entity) override;

	bool hasComponent(eftnc::entity_t entity) override;

	/**
	 * Toggles the given flags for a given entity
	 *
	 * @param entity	Entity ID
	 * @param flags		Flags to toggle, binary OR'd together
	 */
	void toggleFlags(const eftnc::entity_t &, const uint8_t &);

	void onDestroy() override {};

	void setSprite(const eftnc::entity_t &, const std::string &);

	void setAnimation(const eftnc::entity_t &, const std::string &);

	void setScaleX(const eftnc::entity_t &, const float &);

	void setScaleY(const eftnc::entity_t &, const float &);

	void resetAnimation(const eftnc::entity_t &);

private:
	// The components themselves
	std::vector<GraphicsComponent> components;

	//Whether the system should be resorted or not
	bool dirty;

	GraphicResourcesManager * graphicsManager;
	PositionSystem * pos;

	static const std::string graphicsID;
};


#endif //EFTNC_GRAPHICSSYSTEM_H
