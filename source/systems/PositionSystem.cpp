#include "PositionSystem.hpp"
#include <utility>

const std::string PositionSystem::positionID = "position";

void PositionSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
}

const std::string &PositionSystem::getID() {
	return positionID;
}

void PositionSystem::update(sf::Time delta) {
	// Nothing to do
}

void PositionSystem::assemble(eftnc::entity_t entity) {
	if (not components.insert(std::make_pair(entity, PositionComponent {0.0f, 0.0f, 0.0f})).second)
		throw eftnc::generic_exception("Bad insert into position components map");
}

void PositionSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	PositionComponent position;

	position.x = schema.at("x").get<float>();
	position.y = schema.at("y").get<float>();
	position.rotation = schema.at("rotation").get<float>();

	if (not components.insert(std::make_pair(entity, position)).second)
		throw eftnc::generic_exception("Bad insert into position components map");
}

bool PositionSystem::hasComponent(eftnc::entity_t entity) {
	return components.count(entity) > 0;
}

PositionComponent &PositionSystem::getComponent(eftnc::entity_t entity) {
	auto iter = components.find(entity);
	if (iter == components.end())
		throw eftnc::generic_exception("No position component in system");
	return const_cast<PositionComponent &>(iter->second);
}

void PositionSystem::deleteComponent(eftnc::entity_t entity) {
	if (components.erase(entity) <= 0)
		throw eftnc::generic_exception("Tried to erase entity not present in system");
}

void PositionSystem::onDestroy() {
	// Nothing to do
}
