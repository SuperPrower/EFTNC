#ifndef EFTNC_POSITIONSYSTEM_HPP
#define EFTNC_POSITIONSYSTEM_HPP

#include "AbstractSystem.hpp"
#include <map>
#include <Types.hpp>
#include <components/PositionComponent.hpp>

class PositionSystem : public AbstractSystem {
public:
    void onCreate(SystemManager *systemManager) override;

	static const std::string &getID();

    void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	void assemble(eftnc::entity_t entity) override;

	bool hasComponent(eftnc::entity_t entity) override;

	PositionComponent &getComponent(eftnc::entity_t entity);

	void deleteComponent(eftnc::entity_t entity) override;

	void onDestroy() override;

private:
	std::map<eftnc::entity_t, PositionComponent> components;

	static const std::string positionID;
};


#endif //EFTNC_POSITIONSYSTEM_HPP
