#include "SolidSystem.hpp"
#include "CollisionSystem.hpp"
#include "VelocitySystem.hpp"
#include "PositionSystem.hpp"
#include <Shared.hpp>

const std::string SolidSystem::solidID = "solid";

void SolidSystem::onCreate(SystemManager *systemManager) {
	// Add system to systemManager and set up the dependencies
	systemManager->addSystem(getID(), this);
	// The system must execute AFTER Velocity and Collsion
	systemManager->addDependency(getID(), VelocitySystem::getID());
	systemManager->addDependency(getID(), CollisionSystem::getID());
	// The system must execute BEFORE Position
	// Although PositionSystem is not doing anything in its update function element
	// some important systems like Graphics use Position in its dependencies. So
	// the PositionSystem acts like a gate.
	systemManager->addDependency(PositionSystem::getID(), getID());

	// Add groups to Group Manager
	SharedData::groupManager->addGroup("solid");
	SharedData::groupManager->addGroup("static");
	SharedData::groupManager->addGroup("dynamic");
}

const std::string &SolidSystem::getID() {
	return solidID;
}

void SolidSystem::update(sf::Time delta) {
	std::vector<eftnc::entity_t> dynamic;
	std::vector<eftnc::entity_t> solid_static;

	SharedData::groupManager->getEntities("dynamic", dynamic);
	SharedData::groupManager->getEntities("solid static", solid_static);

	CollisionSystem * cs = dynamic_cast<CollisionSystem *>
			(SharedData::systemManager->getSystem(CollisionSystem::getID()));
	PositionSystem * ps = dynamic_cast<PositionSystem *>
			(SharedData::systemManager->getSystem(PositionSystem::getID()));

	// For each dynamic group entity check all its collisions with all solid static entities
	for (auto dynamic_entity : dynamic)
		for (auto solid_static_entity : solid_static)
			if (cs->hasComponent(dynamic_entity) and cs->hasComponent(solid_static_entity)) {
				if (cs->hasIntersection(dynamic_entity, solid_static_entity)) {
					std::pair<float, float> opposite_force = cs->buildIntersectionManifold
							(dynamic_entity, solid_static_entity);
					// If there is a collision, push the dynamic back using the opposite_force vector
					ps->getComponent(dynamic_entity).x += opposite_force.first;
					ps->getComponent(dynamic_entity).y += opposite_force.second;
				}
			}
}

void SolidSystem::assemble(eftnc::entity_t entity) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, "solid static");
}

void SolidSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, schema.at("groups").get<std::string>());
}

bool SolidSystem::hasComponent(eftnc::entity_t entity) {
	return SharedData::groupManager->hasGroups(entity, "solid static")
			or SharedData::groupManager->hasGroups(entity, "dynamic");
}

void SolidSystem::deleteComponent(eftnc::entity_t entity) {
	SharedData::groupManager->removeFromGroups(entity, "solid static dynamic");
}

void SolidSystem::onDestroy() {
	SharedData::groupManager->removeGroup("solid");
	SharedData::groupManager->removeGroup("static");
	SharedData::groupManager->removeGroup("dynamic");
}
