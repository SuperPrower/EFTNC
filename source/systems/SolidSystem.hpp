#ifndef EFTNC_SOLIDSYSTEM_HPP
#define EFTNC_SOLIDSYSTEM_HPP

#include "AbstractSystem.hpp"
#include <Types.hpp>

class SolidSystem : public AbstractSystem {
public:
    void onCreate(SystemManager *systemManager) override;

	static const std::string &getID();

    void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	bool hasComponent(eftnc::entity_t entity) override;

	void deleteComponent(eftnc::entity_t entity) override;

	void onDestroy() override;

private:
	static const std::string solidID;
};


#endif // EFTNC_SOLIDSYSTEM_HPP
