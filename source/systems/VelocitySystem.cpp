#include <string>
#include <utility>
#include "VelocitySystem.hpp"
#include "PositionSystem.hpp"

const std::string VelocitySystem::velocityID = "velocity";

void VelocitySystem::onCreate(SystemManager * systemManager) {
	this->systemManager = systemManager;
	this->systemManager->addSystem(getID(), this);
	// Position must execute AFTER Velocity
	this->systemManager->addDependency(PositionSystem::getID(), getID());
}

const std::string &VelocitySystem::getID() {
	return velocityID;
}

void VelocitySystem::update(sf::Time delta) {
	for (auto &component : components) {
		// Update entity position if exists
		eftnc::entity_t component_id = component.first;
		if (systemManager->hasSystem(PositionSystem::getID())) {
			// Dynamic cast is needed because getSystem returns AbstractSystem pointer
			PositionSystem * ps = dynamic_cast<PositionSystem *>
					(systemManager->getSystem(PositionSystem::getID()));
			// If entity has position component
			if (ps->hasComponent(component_id)) {
				// Update position values
				ps->getComponent(component_id).x += component.second.x_vel * delta.asSeconds();
				ps->getComponent(component_id).y += component.second.y_vel * delta.asSeconds();
			}
		}
	}
}

void VelocitySystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	VelocityComponent vel_component;

	vel_component.x_vel = schema.at("x_vel").get<float>();
	vel_component.y_vel = schema.at("y_vel").get<float>();

	if (not components.insert(std::make_pair(entity, vel_component)).second)
		throw eftnc::generic_exception("Bad insert into velocity component map");
}

void VelocitySystem::assemble(eftnc::entity_t entity) {
	if (not	components.insert(std::make_pair(entity, VelocityComponent {0.0f, 0.0f})).second)
		throw eftnc::generic_exception("Bad insert into velocity component map");
}

bool VelocitySystem::hasComponent(eftnc::entity_t entity) {
	return components.count(entity) > 0;
}

VelocityComponent &VelocitySystem::getComponent(eftnc::entity_t entity) {
	auto iter = components.find(entity);
	if (iter == components.end())
		throw eftnc::generic_exception("No velocity component in system");
	return const_cast<VelocityComponent &> (iter->second);
}

void VelocitySystem::deleteComponent(eftnc::entity_t entity) {
	if (components.erase(entity) <= 0)
		throw eftnc::generic_exception("Tried to erase entity not present in system");
}

void VelocitySystem::onDestroy() {
	// Nothing to do
}
