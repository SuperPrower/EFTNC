#ifndef EFTNC_VELOCITYSYSTEM_HPP
#define EFTNC_VELOCITYSYSTEM_HPP

#include "AbstractSystem.hpp"
#include <Types.hpp>
#include <components/VelocityComponent.hpp>
#include <set>

class VelocitySystem : public AbstractSystem {
public:
	void onCreate(SystemManager * systemManager) override;

	static const std::string &getID();

	void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	bool hasComponent(eftnc::entity_t entity) override;

	VelocityComponent &getComponent(eftnc::entity_t entity_id);

	void deleteComponent(eftnc::entity_t entity_id) override;

	void onDestroy() override;

private:
	std::map<eftnc::entity_t, VelocityComponent> components;

	SystemManager * systemManager;

	static const std::string velocityID;
};


#endif //EFTNC_VELOCITYSYSTEM_HPP
